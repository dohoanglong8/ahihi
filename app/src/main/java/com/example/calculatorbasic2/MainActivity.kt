package com.example.calculatorbasic2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AbsListView
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import net.objecthunter.exp4j.ExpressionBuilder
import java.lang.Exception
import java.lang.NullPointerException
import javax.xml.xpath.XPathExpression

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Number Listener
        b0.setOnClickListener {operatorEvent(true,"0")}
        b1.setOnClickListener {operatorEvent(true,"1")}
        b2.setOnClickListener {operatorEvent(true,"2")}
        b3.setOnClickListener {operatorEvent(true,"3")}
        b4.setOnClickListener {operatorEvent(true,"4")}
        b5.setOnClickListener {operatorEvent(true,"5")}
        b6.setOnClickListener {operatorEvent(true,"6")}
        b7.setOnClickListener {operatorEvent(true,"7")}
        b8.setOnClickListener {operatorEvent(true,"8")}
        b9.setOnClickListener {operatorEvent(true,"9")}
        b00.setOnClickListener { operatorEvent(true,"00") }
        cham.setOnClickListener {operatorEvent(true,".")}

        //Operator listener
        cong.setOnClickListener {operatorEvent(false,"+")}
        tru.setOnClickListener {operatorEvent(false,"-")}
        nhan.setOnClickListener {operatorEvent(false,"*")}
        chia.setOnClickListener {operatorEvent(false,"/")}
        trai.setOnClickListener {operatorEvent(false,"(")}
        phai.setOnClickListener {operatorEvent(false,")")}
        bc.setOnClickListener {
            clear()
        }
        bang.setOnClickListener {
            calculate()
        }
    }
    //create methods
    fun operatorEvent(clear: Boolean,string:String){
        if(clear){
            xuat.text = ""
            nhap.append(string)
        }
        else{
            nhap.append(xuat.text)
            nhap.append(string)
            xuat.text=""
        }
    }
    private fun clear(){
        nhap.text = ""
        xuat.text = ""
    }
    private fun calculate(){
        try {
            val input= ExpressionBuilder(nhap.text.toString()).build()
            val output=input.evaluate()
            val longOutPut= output.toLong()
            if(output==longOutPut.toDouble()){
                xuat.text=longOutPut.toString()
            }
            else{
                xuat.text=output.toString()
            }
        }catch (e:Exception){
            Toast.makeText(this@MainActivity,e.message,Toast.LENGTH_LONG).show()
        }
    }
}